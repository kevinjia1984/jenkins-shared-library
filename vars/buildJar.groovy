#!/user/bin/env groovy

def call() {
    echo 'building the application for branch $BRANCHNAME'
    sh 'mvn package'
}